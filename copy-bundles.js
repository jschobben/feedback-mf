(function () {
    const cpx = require('cpx');
    console.log('Copy UMD bundles ...');

    cpx.copy('node_modules/@angular/*/bundles/*.umd.js', 'src/assets', {}, _ => {});
    cpx.copy('node_modules/rxjs/bundles/*.js', 'src/assets', {}, _ => {});
    cpx.copy('node_modules/zone.js/dist/zone.js', 'src/assets', {}, _ => {});
    // cpx.copy('node_modules/core-js/client/*.js', 'projects/micro-frontend/src/assets', {}, _ => {});
})();
