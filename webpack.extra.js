const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

const webpackRxjsExternals = require('webpack-rxjs-externals');

module.exports = {
  // externals: [
  // {
  //   "rxjs": "rxjs",
  //   "@angular/animations": "ng.animations",
  //   "@angular/common": "ng.common",
  //   "@angular/compiler": "ng.compiler",
  //   "@angular/cdk": "ng.cdk",
  //   "@angular/core": "ng.core",
  //   "@angular/elements": "ng.elements",
  //   "@angular/forms": "ng.forms",
  //   "@angular/platform-browser": "ng.platformBrowser",
  //   "@angular/platform-browser-dynamic": "ng.platformBrowserDynamic",
  //   "@angular/router": "ng.router",
  //   "ng-inline-svg/lib": "ng.inline.svg"
  // },
  //   webpackRxjsExternals()
  // ],
  //   plugins: [
  //       new BundleAnalyzerPlugin({
  //           analyzerMode: 'static'
  //       })
  //   ]
};
