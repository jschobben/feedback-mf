import { AppComponent } from './app.component';
import { CommonModule } from '@angular/common';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import {
  RegasComponentsButtonModule,
  RegasComponentsModalModule,
} from '@regas/components';
import { FeedbackButtonComponent } from './button/feedback-button.component';
import { FeedbackPopupComponent } from './popup/feedback-popup.component';
import { TranslatePipe } from './translations/translate.pipe';

describe('FeedbackComponent', () => {
  let component: AppComponent;
  let fixture: ComponentFixture<AppComponent>;
  let router: Router;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        FormsModule,
        CommonModule,
        ReactiveFormsModule,
        RouterTestingModule,
        RegasComponentsButtonModule,
        RegasComponentsModalModule,
      ],
      declarations: [
        AppComponent,
        FeedbackButtonComponent,
        FeedbackPopupComponent,
        TranslatePipe,
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    router = TestBed.get(Router);

    router.initialNavigation();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('isOpen should be false on default', () => {
    expect(component.isOpen).toBeFalsy();
  });
});
