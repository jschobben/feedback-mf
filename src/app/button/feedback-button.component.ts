import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'regas-feedback-button',
  templateUrl: './feedback-button.component.html'
})
export class FeedbackButtonComponent implements OnInit {
  @Output('feedbackEvent') feedbackEvent = new EventEmitter<void>();

  constructor() {}

  ngOnInit() {}

  toggleFeedbackPopup() {
    this.feedbackEvent.emit();
  }
}
