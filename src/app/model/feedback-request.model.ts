export class FeedbackRequestModel {
  score: string;
  comment?: string;
  url: string;
  domainType: string;

  constructor(score: string, url: string, domainType: string) {
    this.score = score;
    this.url = url;
    this.domainType = domainType;
  }
}
