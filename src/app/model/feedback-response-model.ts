export class FeedbackResponseModel {
  feedbackId: number;
  score: number;
  organisationCode: number;
  comment?: string;
  url: string;
  insertDate: string;
}
