const getCookie = (name: string) =>{
    name += '=';
    const cookielist = document.cookie.split(';');
    const length = cookielist.length;
    for (let i = 0; i < length; i++) {
      let cookie = cookielist[i];
      cookie = cookie.trim();
      if (cookie.indexOf(name) === 0) {
        return cookie.substring(name.length, cookie.length);
      }
    }
    return undefined;
  }

  export default getCookie;