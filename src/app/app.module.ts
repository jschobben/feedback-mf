import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Injector, DoBootstrap, ApplicationRef } from '@angular/core';
import {createCustomElement} from '@angular/elements';
import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
  RegasComponentsButtonModule,
  RegasComponentsModalModule,
  RegasComponentsTableModule,
} from '@regas/components';
import { FeedbackButtonComponent } from './button/feedback-button.component';
import { FeedbackPopupComponent } from './popup/feedback-popup.component';
import { TranslatePipe} from './translations/translate.pipe';
import fr from './translations/fr';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthInterceptor } from './auth.interceptor.service'
import { from } from 'rxjs';

@NgModule({
  declarations: [
    AppComponent,
    FeedbackPopupComponent,
    FeedbackButtonComponent,
    TranslatePipe
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    RegasComponentsModalModule,
    RegasComponentsButtonModule,
    RegasComponentsTableModule,
    ReactiveFormsModule,
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
    { provide: 'window', useValue: window}
  ],
  entryComponents: [
    AppComponent
  ]
})
export class AppModule implements DoBootstrap { 
  constructor(private readonly injector: Injector){ }

    ngDoBootstrap(): void {
      const elm = createCustomElement(AppComponent, {injector: this.injector});
      customElements.define('regas-mf-feedback', elm);
    }
  
}
