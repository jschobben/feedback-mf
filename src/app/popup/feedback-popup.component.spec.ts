import { TranslatePipe } from './../translations/translate.pipe';
import { AppComponent } from './../app.component';
import { CommonModule } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import {
  RegasComponentsButtonModule,
  RegasComponentsModalModule,
} from '@regas/components';
import { FeedbackButtonComponent } from '../button/feedback-button.component';
import { FeedbackPopupComponent } from './feedback-popup.component';
import { FeedbackPopupService } from './feedback-popup.service';
import { FeedbackPopupServiceStub } from './feedback-popup.service.stub';

describe('FeedbackPopupComponent', () => {
  let component: FeedbackPopupComponent;
  let fixture: ComponentFixture<FeedbackPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        CommonModule,
        ReactiveFormsModule,
        HttpClientTestingModule,
        RouterTestingModule,
        RegasComponentsButtonModule,
        RegasComponentsModalModule,
      ],
      declarations: [
        AppComponent,
        FeedbackButtonComponent,
        FeedbackPopupComponent,
        TranslatePipe,
      ],
      providers: [
        { provide: FeedbackPopupService, useClass: FeedbackPopupServiceStub },
        { provide: 'window', useValue: window },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FeedbackPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('form invalid when empty', () => {
    expect(component.formGroup.valid).toBeFalsy();
  });

  it('should from invalid when empty form is submitted', () => {
    component.doCreate();
    expect(component.formGroup.valid).toBeFalsy();
  });

  it('should be valid when a score is submited', () => {
    component.doCreate();
    component.formGroup.setValue({ score: 3, comment: 'something' });
    expect(component.formGroup.valid).toBeTruthy();
    expect(component.formGroup.valid).toBeTruthy();
  });

  it('should emit feedbackevent', () => {
    spyOn(component.feedbackEvent, 'emit');
    component.toggleFeedbackPopup();
    expect(component.feedbackEvent.emit).toHaveBeenCalled();
  });

  it('should not emit feedbackevent', () => {
    spyOn(component.feedbackEvent, 'emit');
    expect(component.feedbackEvent.emit).not.toHaveBeenCalled();
  });
});
