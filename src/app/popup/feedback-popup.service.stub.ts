import { Injectable } from '@angular/core';
import { FeedbackRequestModel } from '../model/feedback-request.model';

@Injectable()
export class FeedbackPopupServiceStub {
  constructor() {}

  public doCreate(score: string, comment: string, url: string) {
    const feedbackRequest = new FeedbackRequestModel(score, url, 'web-based');
    feedbackRequest.comment = comment;
  }
}
