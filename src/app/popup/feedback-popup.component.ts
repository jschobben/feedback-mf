import {
  Component,
  EventEmitter, Inject,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { FormControl, FormGroup, NgForm, Validators } from '@angular/forms';
import de from '../translations/de';
import en from '../translations/en';
import fr from '../translations/fr';
import nl from '../translations/nl';
import {FeedbackPopupService} from './feedback-popup.service';

@Component({
  selector: 'regas-feedback-popup',
  templateUrl: './feedback-popup.component.html',
})
export class FeedbackPopupComponent implements OnInit {
  @Output('feedbackEvent')
  feedbackEvent = new EventEmitter<void>();

  isFeedbackSubmitted: boolean;
  translations = {
    en,
    fr,
    de,
    nl,
  };
  showError: boolean;

  formGroup: FormGroup;

  constructor(
    private feedbackPopupService: FeedbackPopupService,
    @Inject('window') private readonly window: Window
  ) {}

  ngOnInit(): void {
    this.isFeedbackSubmitted = false;
    this.formGroup = new FormGroup({
      score: new FormControl(undefined, Validators.required),
      comment: new FormControl(undefined, Validators.maxLength(2000)),
    });
  }

  doCreate() {
    this.showError = true;
    if (this.formGroup.valid) {
      this.feedbackPopupService.doCreate(
        this.formGroup.value.score,
        this.window.location.pathname,
        this.formGroup.value.comment,
      );
      this.isFeedbackSubmitted = true;
    }
  }

  toggleFeedbackPopup() {
    this.feedbackEvent.emit();
  }
}
