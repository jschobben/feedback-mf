import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FeedbackRequestModel } from '../model/feedback-request.model';
import { FeedbackSettings } from '../settings/FeedbackSettings';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class FeedbackPopupService {
  constructor(
    private readonly http: HttpClient,
  ) {}

  public doCreate(score: string, url: string, comment?: string) {
    const feedbackRequest = new FeedbackRequestModel(score, url, 'web-based');

    if (comment) {
      feedbackRequest.comment = comment;
    }
    this.http
      .post<string>(environment.host+'/feedback',
        JSON.stringify(feedbackRequest),
      )
      .subscribe();
  }
}
