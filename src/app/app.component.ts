import { Component, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss',],
  encapsulation: ViewEncapsulation.ShadowDom
})
export class AppComponent {
  isOpen: boolean;

  constructor() {}

  toggleFeedbackPopup(): void {
    this.isOpen = !this.isOpen;
  }}
