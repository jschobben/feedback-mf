export default {
  feedback: {
    title: 'Wat vind je van het nieuwe ontwerp van REGAS? ',
    body: 'Wat vind je van het nieuwe ontwerp op een schaal van 1 tot 5?',
    no_rating: 'Klik op één van de sterren om een score te geven',
    comment_label: 'Onderbouwing score',
    thanks_header: 'Bedankt voor je feedback',
    thanks: 'We gebruiken je feedback om REGAS nog beter te maken.',
    close: 'Afsluiten',
    save: 'Verzenden',
    disclaimer:
      'Wij van Regas willen graag weten wat je van ons nieuwe ontwerp vindt (geen inhoudelijke meldingen of bugs). Feedback is inzichtelijk voor Regas en gerechtigden bij jouw organisatie, verder delen we dit met niemand.',
  },
};
