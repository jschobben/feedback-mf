import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'translate',
})
export class TranslatePipe implements PipeTransform {
  language: string;
  default = 'nl';

  constructor() {
  }

  transform(keyword: string, translations: any): string {
    const path = keyword.split('.');

    /**
     * We need to get the right translation. We iterate through the languageObject until are words in path are used.
     * If there is no object to iterate through return the keyword, so the developer can see they made a typo.
     */

    let translation: any =
        translations[this.language] || translations[this.default];

    for (const step of path) {
      if (!step) {
        return translation;
      }

      translation = translation[step];

      if (!translation) {
        return keyword;
      }
    }
    if (typeof translation === 'object') {
      // the path is missing some keywords.
      return `[ ${keyword} ] returns an object, not a string`;
    }
    return translation;
  }
}
