export default {
  feedback: {
    title: 'Was halten Sie von dem neuen REGAS Design?',
    body: 'Wie bewerten Sie das neue Design?',
    no_rating: 'Bitte wählen Sie einen Stern, um das Design zu bewerten',
    comment_label: 'Erklärung Punktzahl',
    thanks_header: 'Vielen Dank für Ihr Feedback',
    thanks: 'Wir werden Ihr Feedback nutzen, um REGAS zu verbessern.',
    save: 'Bewahren',
    close: 'Schließen',
    disclaimer: `Wir von Regas würden gerne wissen, was Sie von unserem neuen Design halten (keine inhaltlichen Fragen oder Fehler). Feedback ist nur für Regas und Empfänger in Ihrer Organisation zugänglich, wir teilen dies nicht mit anderen.`,
  },
};
