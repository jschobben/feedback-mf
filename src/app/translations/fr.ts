export default {
  feedback: {
    title: 'Que pensez-vous du nouveau design REGAS?',
    body: 'Comment évaluez-vous le nouveau design ?',
    no_rating: 'Veuillez sélectionner une étoile pour évaluer le design',
    comment_label: 'Explication score',
    thanks_header: 'Merci pour vos commentaires',
    thanks: 'Nous utiliserons vos commentaires pour perfectionner REGAS.',
    close: 'Fermer',
    save: 'Sauvegarder',
    disclaimer: `Chez Regas, nous aimerions savoir ce que vous pensez de notre nouveau design (pas de questions de fond ni de bugs). Les commentaires ne peuvent être vus que par Regas et des personnes de votre organisation ayant le droit de les voir, nous ne les partageons avec personne.`,
  },
};
