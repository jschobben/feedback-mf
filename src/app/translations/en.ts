export default {
  feedback: {
    title: 'What do you think of the new REGAS design?',
    body: 'How do you rate the new design?',
    no_rating: 'Please select a star to rate the design',
    comment_label: 'explanation score',
    thanks_header: 'Thank you for your feedback.',
    thanks: 'We will use your feedback to improve REGAS.',
    save: 'Save',
    close: 'close',
    disclaimer:
      'We at Regas would like to know what you think of our new design (no substantive reports or bugs). Feedback is only available to Regas and authorized personnel in your organization, we do not share this with others.',
  },
};
