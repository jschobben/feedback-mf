import { Inject, Injectable } from '@angular/core';

@Injectable()
export abstract class AuthorisationAbstractService {
  /**
   * Authorization is stored in a cookie. The name of this cookie differs between the environments. So the constructor
   * needs the name of this cookie
   * @param {string} cookieName Name of the cookie containing the authorization bearer
   */
  constructor(@Inject('cookieName') private cookieName: string) {}

  /**
   * append an Authorization bearer to the given header
   */
  protected getAuthorizationToken(): string {
    return `Bearer ${this.getCookie(this.cookieName)}`;
  }

  /**
   * Get the content of the given cookie
   * @param {string} cookieName name of the cookie to get the content from
   * @returns {string} content of the given cookie
   */
  protected getCookie(cookieName: string): string {
    const cookies: string[] = document.cookie.split(';');
    cookieName += '=';

    for (let cookie of cookies) {
      cookie = cookie.replace(/^\s+/g, '');
      if (cookie.indexOf(cookieName) === 0) {
        return cookie.substring(cookieName.length, cookie.length);
      }
    }
    return '';
  }
}
