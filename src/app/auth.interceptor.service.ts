import { Injectable, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import {HttpEvent, HttpHandler, HttpHeaders, HttpRequest} from "@angular/common/http";
import {Observable} from "rxjs";
import getCookie from './getCookie';


@Injectable({
  providedIn: 'root'
})
export class AuthInterceptor {
  constructor(
    @Inject(DOCUMENT) private readonly document: any,
) {}

  intercept(
      req: HttpRequest<any>,
      next: HttpHandler,
  ): Observable<HttpEvent<any>> {

      const clone: HttpRequest<any> = req.clone({
          headers: new HttpHeaders({
              "Authorization": `Bearer ${getCookie('REGASToken')}`,
              "Content-Type": "application/json"
          })
      });

      return next.handle(clone);
  }
}
