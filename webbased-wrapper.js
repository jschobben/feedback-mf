/*
 * This File is wrapper for webbased to encapsulate styling.
 * We use shadow dom for it (not supported in IE 11)
 */

 const microFrontend = document.createElement('regas-mf-feedback');
 const style = document.createElement('link');

style.rel = "stylesheet";
style.href = "/regas-styling.css";
console.log(style)

 const element = document.createElement('div');
 const shadow  = element.attachShadow({mode: "closed"});
 
 shadow.appendChild(style);
 shadow.appendChild(microFrontend);


 document.appendChild(element);