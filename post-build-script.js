const fs = require('fs');
const path = require('path');
const rimraf = require("rimraf");
const {name} = require("./package.json")

const environmet = process.argv[2];

const buildDir = __dirname+'\\files-project\\' + environmet
const scriptsDir = __dirname+'\\dist\\'+ name

console.log("Clean up old build");

if (fs.existsSync(buildDir)){
    rimraf.sync(buildDir);
}

fs.mkdirSync(buildDir);

console.log("moving files");

fs.renameSync(scriptsDir + '\\main-es2015.js', buildDir + '\\' + name + ".js",(err) => {console.log(err)});

fs.copyFileSync(__dirname+ '\\files-project\\nginx.conf', __dirname+ '\\files-project\\'+ environmet +'\\nginx.conf')

console.log("DONE")